﻿using Newtonsoft.Json;
using System;
using System.Data.Objects;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DataAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class DataController : ApiController
    {
        ExamAxityEntities db = new ExamAxityEntities();

        [HttpGet]
        [Route("loggin/{usr}/{psw}")]
        public string loggin(string usr, string psw)
        {
            var response = new ObjectParameter("resp", typeof(string));
            try
            {
                var exec = db.SP_LOGGIN(usr, psw, response);
            }
            catch (Exception)
            {

                throw;
            }

            return JsonConvert.SerializeObject(new { response.Value });
        }

        [HttpGet]
        [Route("products/")]
        public string getProducts()
        {
            var listProducts = db.Products.ToList();
            return JsonConvert.SerializeObject(new { listProducts });
        }
    }
}
